/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import MaterialTable from "material-table";
import axios from 'axios';
const Notifications = (props) => {
  const [flag, setflag] = useState(true);
  const [data, setdata] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  useEffect(() => {
    if (flag === true) {
      setflag(false)
      const options = {
        method: 'GET',
        url: "https://api.rootnet.in/covid19-in/notifications",
      };
      console.log(options)
      axios(options)
        .then(response => {
          // console.log(response.data.data.notifications)
          response.data.data.notifications.map((notification, i) => {
            console.log (notification.title)
            console.log (notification.title.substring(5,4))
            if (isNaN(notification.title.substring(0, 1)) == true || isNaN(notification.title.substring(5,4)) == true ) {
              console.log("__+++++++++++++++++++++++++++++++++++++++++++++++++++=")
              console.log(notification.title)
              var obj = notification;
              Object.assign(obj, { no: i + 1 });
              Object.assign(obj, { date: "N/A" });
              if (i == response.data.data.notifications.length - 1) {
                setdata(response.data.data.notifications)
              }
            }
            else {
              var obj = notification;
              Object.assign(obj, { no: i + 1 });
              Object.assign(obj, { date: notification.title.substring(0, 10) });
              if (i == response.data.data.notifications.length - 1) {
                setdata(response.data.data.notifications)
              }
            }
          })
        })
    }
  })
  return (
    <div>
      <Typography variant="h4" style={{ color: '#0A97B0', fontWeight: 'bold', textAlign: 'center' }}>
        <h4 style={{ textAlign: 'center' ,fontFamily: 'Source Sans Pro' }}><u>COVID-19</u></h4>
        <h4 style={{ textAlign: 'center',fontFamily: 'Source Sans Pro'  }}><u>Notifications & advisories</u></h4>
      </Typography>
      <Grid style={{ marginTop: "6%" }}>
        <MaterialTable
          title="Notifications & advisories"
          columns={[
            { title: 'S.No', field: 'no' },
            { title: 'Date', field: 'date' },
            {
              title: 'Notification Title', field: 'title', render: rowData =>
                <div>
                  {
                    rowData.title.length > 10 ? (
                      <p>{rowData.title.substring(10, rowData.length)}</p>
                    ) : (
                        <p>{rowData.title}</p>
                      )
                  }
                </div>
            },
            {
              title: 'Link', field: 'link', render: rowData => <a href={rowData.link}>{rowData.link}</a>
            },
          ]}
          data={data}
          onRowClick={((evt, selectedRow) => setSelectedRow(selectedRow.tableData.id))}
          options={{
            actionsColumnIndex: -1,
            headerStyle: {
              // background: 'linear-gradient( #1499db, #82ffa1)',
              background: '#1499db',
              color: '#FFF'
            },
            rowStyle: {
              backgroundColor: '#e9f7f3',
            },
            rowStyle: rowData => ({
              backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
            }),
            sorting: true,
            exportButton: true,
            exportAllData: true,
            pageSize: 5
          }}
          column={{
            cellStyle: rowData => ({
              backgroundColor: (rowData.tableData.id) ? '#EEE' : '#FFF'
            })
          }}
          localization={{
            header: {
              actions: 'Action'
            },
          }}
        />
      </Grid>
    </div>
  );
}
export default Notifications;
