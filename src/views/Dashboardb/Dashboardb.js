/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import MaterialTable from "material-table";
import axios from 'axios';

const Dashboard = (props) => {
  const [flag, setflag] = useState(true);
  const [data, setdata] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);

  useEffect(() => {
    if (flag === true) {
      setflag(false)
      const options = {
        method: 'GET',
        url: "https://api.rootnet.in/covid19-in/hospitals/medical-colleges",
      };
      console.log(options)
      axios(options)
        .then(response => {

          console.log(response.data.data.medicalColleges)
          response.data.data.medicalColleges.map((Medbeds, i) => {

            var obj = Medbeds;
            Object.assign(obj, { no: i + 1 });
            console.log(i)
            if (i == response.data.data.medicalColleges.length - 1) {
              setdata(response.data.data.medicalColleges)
            }
            

          })
        })
    }
  })

  return (
    <div>
      <Typography variant="h4" style={{ color: '#0A97B0', fontWeight: 'bold', textAlign: 'center' }}>
        <h4 style={{ textAlign: 'center' ,fontFamily: 'Source Sans Pro' }}><u>COVID-19</u></h4>
        <h4 style={{ textAlign: 'center',fontFamily: 'Source Sans Pro'  }}><u>Medical Colleges & beds</u></h4>
      </Typography>
      <Grid style={{ marginTop: "6%" }}>
        <MaterialTable
          title="Medical Colleges & beds details"
          columns={[
            { title: 'S.No', field: 'no' },
            { title: 'State Name', field: 'state' },
            { title: 'Institute Name', field: 'name' },
            { title: 'City', field: 'city' },
            { title: 'Type', field: 'ownership' },
            { title: 'Admission Capacity', field: 'admissionCapacity' },
            { title: 'Hospital Beds', field: 'hospitalBeds' },


          ]}
          data={data}
          onRowClick={((evt, selectedRow) => setSelectedRow(selectedRow.tableData.id))}
          options={{
            actionsColumnIndex: -1,
            headerStyle: {
              // background: 'linear-gradient( #1499db, #82ffa1)',
              background: '#1499db',
              color: '#FFF'
            },
            rowStyle: {
              backgroundColor: '#e9f7f3',
            },
            rowStyle: rowData => ({
              backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
            }),
            sorting: true,
            exportButton: true,
            exportAllData: true,
            pageSize: 5
          }}
          column={{
            cellStyle: rowData => ({
              backgroundColor: (rowData.tableData.id) ? '#EEE' : '#FFF'
            })
          }}
          localization={{
            header: {
              actions: 'Action'
            },
          }}
        />
      </Grid>
    </div>
  );
}
export default Dashboard;
