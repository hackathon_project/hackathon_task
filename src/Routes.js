/* eslint-disable */
import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Contact,
  Notifications,
  Dashboards,
  Dashboardsb  
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/contact"
      />
    
      <RouteWithLayout
        component={Contact}
        exact
        layout={MainLayout}
        path="/contact"
      />

      <RouteWithLayout
        component={Notifications}
        exact
        layout={MainLayout}
        path="/notifications"
      />
      <RouteWithLayout
        component={Dashboards}
        exact
        layout={MainLayout}
        path="/dashboards"
      />
       <RouteWithLayout
        component={Dashboardsb}
        exact
        layout={MainLayout}
        path="/dashboardsb"
      />
      <RouteWithLayout
        component={<Redirect to="/login" />}
        exact
        layout={MainLayout}
        path="/login"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
